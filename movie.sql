-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Apr 24, 2021 at 05:03 AM
-- Server version: 10.4.13-MariaDB
-- PHP Version: 7.3.21

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `movie`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_seats`
--

DROP TABLE IF EXISTS `tbl_seats`;
CREATE TABLE IF NOT EXISTS `tbl_seats` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `seat` varchar(255) NOT NULL,
  `movie` varchar(255) NOT NULL,
  `status` varchar(20) NOT NULL DEFAULT 'Reserved',
  `time` varchar(255) NOT NULL,
  `date` date NOT NULL,
  `price` int(255) NOT NULL,
  `log` datetime NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=33 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_seats`
--

INSERT INTO `tbl_seats` (`id`, `name`, `seat`, `movie`, `status`, `time`, `date`, `price`, `log`) VALUES
(25, 'bret fabian', '2A', 'Kargada ni Abel', 'occupied', '9:30pm', '2021-04-30', 170, '2021-04-23 14:50:52'),
(29, 'papa mo hatdog', '5E', 'Umaga na ng Hinugot', 'occupied', '9:30pm', '2021-04-30', 160, '2021-04-23 15:29:05'),
(30, 'testing me all the way', '6C', 'Malagkit na Hardin', 'occupied', '10:30pm', '2021-04-24', 150, '2021-04-23 21:35:05'),
(22, 'master tan', '1A', 'Kargada ni Abel', 'occupied', '10:30pm', '2021-04-24', 170, '2021-04-22 22:59:34'),
(32, 'bret fabian', '4C', 'Adan', 'occupied', '10:30pm', '2021-04-14', 190, '2021-04-24 13:00:50'),
(28, 'papa mo hatdog', '5D', 'Umaga na ng Hinugot', 'occupied', '9:30pm', '2021-04-22', 160, '2021-04-23 14:53:24');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

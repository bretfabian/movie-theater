<?php
    require_once 'php/init.php';
    error_reporting (E_ALL ^ E_NOTICE); 
?>

<!DOCTYPE html>
<html lang="en">
<head>
  <title>Odeoncinema.com</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" type="text/css" href="vendor/css/bootstrap.min.css">
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css" integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous">
  <link href="vendor/css/all.css" rel="stylesheet">
  <link rel="stylesheet" type="text/css" href="resource/css/style.css">
  <link rel="preconnect" href="https://fonts.gstatic.com">
<link href="https://fonts.googleapis.com/css2?family=Lato:ital,wght@0,300;0,400;0,700;0,900;1,100;1,300;1,400;1,700;1,900&display=swap" rel="stylesheet"/>
</head>

<body>
  <header id="home">
  <div class="container-fluid navcon">
    <div class="containerg">
      <nav class="navbar navbar-dark pt-md-1 mb-1 navbar">
        <div class="row">
        <h5 class=" mt-2 brand">
            <span id="Wampip">odeon</span>
            <span id="TEA">Cinema</span>
        </h5>
      </nav>
      </div>
  </div>
     <div class="containers">
  <div class="jumbotron jumbotron-fluid header-text">
      <div class="container">
        <h1 class="headermain">
          <span>A different Viewing</span><br/> 
          <span class="sipsip">Experience</span>
        </h1>
      </div>
  </div>
  </div>

<div class="containery pt-md-1">
      <div class="row">
        <div class="col-md-3">
          <div class="card cardcon .h-100">
            <h3>Patikim ng Pinya</h3>
            <img src="resource/img/3.jpg" class="card-img-top img" alt="pinya">
            <h4><span class="rate">R-18</span><span class="presyo">P170</span>
              </h4>
            <div class="car-body">
            <p class="card-text text-justify">This is the story of a seductive woman, Myra (Rosanna Roces), who as a fruit vendor finds it to her advantage to be charming and gracious to men. But it is to a young stranger with whom she falls in love.</p>
            <a href="index.php" class="btn-card">Book Now</a>
            </div>
          </div>
        </div>
        <div class="col-md-3">
          <div class="card cardcon .h-100">
            <h3>Adan</h3>
            <img src="resource/img/4.jpg" class="card-img-top img" alt="adan">
            <h4><span class="rate">R-18</span><span class="presyo">P190</span>
              </h4>
            <div class="car-body">
            <p class="card-text text-justify">A naive provincial girl finds a way to get ahold of her independence through the help of her girl best friend. But their actions have nerve-shattering consequences with whom she falls in love. what will happen next will suprise you.</p>
            <a href="index.php" class="btn-card">Book Now</a>
            </div>
          </div>
        </div>
        <div class="col-md-3">
          <div class="card cardcon .h-100">
            <h3>Hibla</h3>
            <img src="resource/img/5.jpg" class="card-img-top img" alt="hibla">
            <h4><span class="rate">R-18</span><span class="presyo">P160</span>
              </h4>
            <div class="car-body">
            <p class="card-text text-justify">two childhood friends separated by fate. Isabel is a naive provincial lass who works as an indigenous fabric weaver while Clara is the liberated city girl. Clara has gone wild after her mother runs off with another man.</p>
            <a href="index.php" class="btn-card">Book Now</a>
            </div>
          </div>
        </div>
        <div class="col-md-3">
          <div class="card cardcon .h-100">
            <h3>Silip</h3>
            <img src="resource/img/6.jpg" class="card-img-top img" alt="silip">
            <h4><span class="rate">R-18</span><span class="presyo">P150</span>
              </h4>
            <div class="car-body">
            <p class="card-text text-justify">Small-town waitress Tess is swept off her feet by dashing salesman Rico, who marries her and takes her to live on his isolated farm. When Rico goes on a sales trip and leaves her all alone, Tess fights off loneliness by befriending Celia.</p>
            <a href="index.php" class="btn-card">Book Now</a>
            </div>
          </div>
        </div>
        
      </div>
    </div>

</div>

  </header>

   <footer>
    <div class="container">
      <div class="row">
        <div class="col-md">
          <p class="ft2">
            CTTO for pictures, For educational Purposes Only<br/>&nbsp;&nbsp;&nbsp;
            all rights reserve 2021<br/>&nbsp;&nbsp;&nbsp;
            <i class="fab fa-facebook-square"></i>
            <i class="fab fa-twitter-square"></i>
            <i class="fab fa-instagram"></i>
            <i class="fab fa-youtube"></i>
          </p>
        </div>
      </div>
    </div>
  </footer>
  <script src="resource/js/script.js"></script>
  <script src="vendor/js/jquery.js"></script>
  <script src="vendor/js/popper.js"></script>
  <script src="vendor/js/bootstrap.min.js"></script>
</body>

</html>